
// Integers
var x = 14;
var y = 3;

console.log("Integer Calculations, x = " + x + ", y = " + y + "\n");

console.log("x + y = " + (x + y)); // Addition
console.log("x - y = " + (x - y)); // Subtraction
console.log("x * y = " + (x * y)); // Multiplication
console.log("x / y = " + (x / y)); // Division
console.log("x % y = " + (x % y)); // Modulus
console.log("x ** y = " + (x ** y)); // Exponent

// Floating point
var x = 14.5;
var y = 3.2;

console.log("\nFloating point Calculations, x = " + x + ", y = " + y + "\n");

console.log("x + y = " + (x + y)); // Addition
console.log("x - y = " + (x - y)); // Subtraction
console.log("x * y = " + (x * y)); // Multiplication
console.log("x / y = " + (x / y)); // Division
console.log("x % y = " + (x % y)); // Modulus
console.log("x ** y = " + (x ** y)); // Exponent

// Strings
console.log("\nString Manipulation\n");

var hello = "hello";
console.log(hello);

hello = hello + " world";
console.log(hello);

console.log(hello.substring(2, 6)); // substring

// Arrays
var numbers = [3, 2, 8, 6, 8, 1];
console.log("\nArray Manipulation\n");

console.log(numbers);
console.log(numbers.length);

numbers.push(100);
console.log(numbers);

numbers.splice(0, 1);
console.log(numbers);

