var running = true;
var i = 0;

// while loop
while(running)
{
    i += 1;
    if(i > 5)
    {
        running = false;
    }
}

// for loop
for(var i = 0; i < 13; i++)
{
    console.log("13 * " + i + " = " + 13 * i);
}

var list = [2, 1, 4, 5];

// for/in loop
for(var i in list)
{
    console.log(list[i]);
}

