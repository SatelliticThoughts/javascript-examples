// Hello World example
console.log("Hello World!");

// variable example
var text = "This is a test";
console.log(text);

// multiple variables example
var name = "luke";
var age = 300;

console.log("My name is " + name + " and i am " + age + " years old");

