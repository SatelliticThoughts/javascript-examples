// Basic function with no return statement or parameters
function sayHello()
{
    console.log("Hello");
}

// Function with return statement
function getWord()
{
    return "dog";
}

// Function with single parameter
function doubleNumber(x)
{
    console.log(x * 2);
}

// Function with multiple parameters and return statement
function addNumbers(x, y)
{
    return x + y;
}

// calling functions
sayHello();
console.log(getWord());
doubleNumber(6);
console.log(addNumbers(3, 1));
