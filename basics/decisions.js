// Basic if statement
if(true)
{
    console.log("This will show")
}

// Basic if/else statement
if(false)
{
    console.log("This will NOT show");
}

// Basic if/else if/else statement
if(false)
{
    console.log("This will NOT show");
}
else if(true)
{
    console.log("This will show");
}
else
{
    console.log("This will NOT show");
}

// &&, and
if(true && true)
{
    console.log("This will show");
}

if(true && false)
{
    console.log("This will NOT show");
}

if(false && false)
{
    console.log("This will NOT show");
}

// ||, or
if(true || true)
{
    console.log("This will show");
}

if(true || false)
{
    console.log("This will show");
}

if(false || false)
{
    console.log("This will NOT show");
}


var x = 80;
var y = 10;

// Equals
if(x == y)
{
    console.log("If x equals y this will show");
}

// Not Equal
if(x != y)
{
    console.log("If x does not equal y this will show");
}

// Greater Than
if(x > y)
{
    console.log("If x is greater than y this will show");
}

// Less Than
if(x < y)
{
    console.log("If x is less than y this will show");
}
