# javascript-examples

javascript-examples contains code to demonstrate various things in javascript.

## Set up

After cloning or downloading, most programs should be a single file that can be run with node, unless otherwise stated.

```
node 'filename.js' # run file
```

## License
[MIT](https://choosealicense.com/licenses/mit/)
